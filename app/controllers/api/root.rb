module API
  class Root < Grape::API
    prefix ''
    mount API::V1::Root
  end
end
