module API
  module V1
    module Defaults
      extend ActiveSupport::Concern

      def self.api_error(api, e, message, status)
        Rollbar.error(e) if e
        api.error!({error: message}, status)
      end

      included do
        version 'v1'
        format :json

        helpers do

          def api_params
            @api_params ||= ActionController::Parameters.new(params)
          end

        end

        # Not found error
        rescue_from ActiveRecord::RecordNotFound do |e|
          API::V1::Defaults.api_error(self, nil, e.message, 404)
        end

        # CanCanCan abilities check
        rescue_from CanCan::AccessDenied do
          error!('401 Unauthorized', 401)
        end

        # Validation Errors
        rescue_from Grape::Exceptions::ValidationErrors, ActiveRecord::RecordInvalid do |e|
          message = e.message
          message = e.full_messages.first if e.instance_of?(Grape::Exceptions::ValidationErrors)
          API::V1::Defaults.api_error(self, nil, message, 422)
        end

        # global exception handler, used for error notifications
        rescue_from :all do |e|
          if Rails.env.development? || Rails.env.test?
            message = "Internal server error: #{e.message}"
          else
            message = 'Internal server error'
          end
          API::V1::Defaults.api_error(self, e, message, 500)
        end
      end
    end
  end
end
