require 'grape-swagger'

module API
  module V1
    class Root < Grape::API
      add_swagger_documentation(
        api_version: 'v1',
        version: 'v1',
        hide_documentation_path: true,
        hide_format: true,
        format: :json,
      )
    end
  end
end
