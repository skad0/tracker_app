Rails.application.routes.draw do
  devise_for :users
  root to: 'home#index'

  mount API::Root, at: '/'

  mount GrapeSwaggerRails::Engine, at: '/swagger'
end
