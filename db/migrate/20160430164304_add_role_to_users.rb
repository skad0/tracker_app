class AddRoleToUsers < ActiveRecord::Migration
  def change
    execute <<-SQL
      CREATE TYPE role AS ENUM ('user', 'carrier', 'admin');
    SQL

    add_column :users, :role, :role, index: true
  end
end
